<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\Kernel;

class CheckWordCommand extends ContainerAwareCommand
{
    private $badWords = [];

    protected function configure()
    {
        $this
            ->setName('app:check-words')
            ->setDescription('Check words.')
            ->setHelp('This command allows you to check words...')
            ->addOption('input', 'i', InputOption::VALUE_REQUIRED, 'Specify the input string to check.')
            ->addOption('algorithm', 'a', InputOption::VALUE_OPTIONAL, 'Specify the algorithm version to use.')
            ->addOption('force', 'f', InputOption::VALUE_OPTIONAL, 'Specify to force search until end of words.')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $string = strtolower($input->getOption('input'));
        $version = (int)$input->getOption('algorithm');
        $force = $input->getOption('force');
	$force = ($force === null) ? true : (bool)(int)$force;

        /** @var Kernel $kernel */
        $kernel = $this->getContainer()->get('kernel');
        $wordsPath = $kernel->locateResource('@AppBundle/Resources/files/words.txt');
        $this->badWords = explode("\n", file_get_contents($wordsPath));

        if ($version === 0) {
            $this->badWords = array_flip($this->badWords);
        }

        $time = microtime(true);

        $function = 'validate' . $version;

        if (!method_exists($this, $function)) {
            return;
        }

        $res = $this->$function($string, $force);

        $output->writeln(
            sprintf(
                'Risposta: "%s" e sono passati %fµs',
                $res ? 'true' : 'false',
                microtime(true) - $time
            )
        );
    }

    /**
     * @param string $input
     * @param bool $force
     * @return bool
     */
    private function validate0(string $input, bool $force = true) : bool
    {
        $input = explode(' ', $input);

        $found = false;
        foreach ($input as $s) {
            if (isset($this->badWords[$s])) {
                if (!$force) {
                    return true;
                }
                $found = true;
            }
        }

        return $found;
    }

    /**
     * @param string $input
     * @param bool $force
     * @return bool
     */
    private function validate1(string $input, bool $force = true) : bool
    {
        return preg_match('~\b(' . implode('|', $this->badWords) . ')\b~', $input);
    }

    /**
     * @param string $input
     * @param bool $force
     * @return bool
     */
    private function validate2(string $input, bool $force = true) : bool
    {
        $found = false;
        foreach ($this->badWords as $word) {
            if (empty($word)) {
                continue;
            }

            if (strpos($input, $word) !== false) {
                if (!$force) {
                    return true;
                }
                $found = true;
            }
        }

        return $found;
    }

    /**
     * @param string $input
     * @param bool $force
     * @return bool
     */
    private function validate3(string $input, bool $force = true) : bool
    {
        $input = explode(' ', $input);

        $found = false;
        foreach ($input as $s) {
            foreach ($this->badWords as $w) {
                if ($w === $s) {
                    if (!$force) {
                        return true;
                    }
                    $found = true;
                }
            }
        }

        return $found;
    }

    /**
     * @param string $input
     * @param bool $force
     * @return bool
     */
    private function validate4(string $input, bool $force = true) : bool
    {
        $input = explode(' ', $input);

        $found = false;
        foreach ($input as $s) {
            if (in_array($s, $this->badWords)) {
                if (!$force) {
                    return true;
                }
                $found = true;
            }
        }

        return $found;
    }

    /**
     * @param string $input
     * @param bool $force
     * @return bool
     */
    private function validate5(string $input, bool $force = true) : bool
    {
        return count(
            array_filter(
                $this->badWords,
                function ($v) use ($input) {
                    if (empty($v)) {
                        return false;
                    }

                    return strstr($input, $v);
                }
            )
        ) > 0;
    }
}
